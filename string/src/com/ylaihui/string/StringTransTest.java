//StringTransTest.java
package com.ylaihui.string;

import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public class StringTransTest {
    @Test
    public void test1(){
        String s1 = "123";
        int parseInt = Integer.parseInt(s1);
        System.out.println(parseInt);

        int i = 123;
        String s2 = String.valueOf(i);
        System.out.println(s2);
    }
    @Test
    public void test2(){
        String s1 = "ylaihui";
        char[] chars = s1.toCharArray();
        for (int i = 0; i <chars.length; i++) {
            System.out.println(chars[i]);
        }

        String s2 = new String(chars);
        System.out.println(s2);
    }
    @Test
    public void test3() throws UnsupportedEncodingException {
        // 编码
        String s1 = "ylaihui猿来绘";
        byte[] bytes = s1.getBytes(); // 使用默认的字符集 UTF-8
        System.out.println(Arrays.toString(bytes));  // 与 UTF-8 字符集有关

        byte[] gbks = s1.getBytes("gbk");

        // 解码
        String s2 = new String(bytes);
        System.out.println(s2);
        String s3 = new String(gbks);
        System.out.println(s3);  //出现乱码。原因：编码集和解码集不一致！

        String s4 = new String(gbks, "gbk");
        System.out.println(s4);
    }
    @Test
    public void test4(){
        String s1 = "ylaihui";
        String s2 = "ylai";
        String s3 = s2 + "hui";
        System.out.println(s1 == s3); // false

        String s4 = "ylaihui";
        final String s5 = "ylai";
        String s6 = s5 + "hui";
        System.out.println(s4 == s6); // true
    }

}
