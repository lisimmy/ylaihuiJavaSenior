//StringMethod.java
package com.ylaihui.string;

import org.junit.Test;

public class StringMethod {
    @Test
    public void test1(){
        String s1 = "lisimmy";
        String s2 = s1.toUpperCase();
        System.out.println(s1);  // 字符串的不可变性，未对 s1 修改
        System.out.println(s2);
    }
    @Test
    public void test2(){
        String s1 = "abc";
        String s2 = "abd";
        int i = s1.compareTo(s2); // 理解 s1 - s2
        System.out.println(i);
    }
    @Test
    public void test3(){
        String s1 = "lisimmy";
        String s2 = s1.substring(2);
        System.out.println(s2);

        String s3 = s1.substring(2,4); // [2,4) 前闭后开
        System.out.println(s3);
    }
    @Test
    public void test4(){
        String s1 = "ylaihui";
        System.out.println(s1.startsWith("ylai"));
        System.out.println(s1.startsWith("ai",2));  // 从索引位置 2 开始的字串是否 以 ai 开头
        System.out.println(s1.endsWith("hui"));
    }

    @Test
    public void test5(){
        String s1 = "ylaihui";
        System.out.println(s1.contains("lai"));
        System.out.println(s1.indexOf("hui"));  // 以 "hui" 开始的字符串的索引位置 -> 4
        System.out.println(s1.indexOf("hui", 3)); // 从索引为3 位置开始找， 找"hui" 字符串 -> 4

        String s2 = "ylaihuiylaihui";
        System.out.println(s2.lastIndexOf("hui")); // -> 11
        System.out.println(s2.lastIndexOf("hui", 8)); // 从 索引 8 开始，往左找字符串 "hui"  -> 4
    }
    @Test
    public void test6(){
        String str1 = "猿来绘商城";
        String str2 = str1.replace('城', '品');

        System.out.println(str1);
        System.out.println(str2);

        String str3 = str1.replace("商城", "博客");
        System.out.println(str3);

        String str = "123www1234ylaihui4567com543";
        //把字符串中的数字替换成,，如果结果中开头和结尾有，的话去掉
        String string = str.replaceAll("\\d+", ".").replaceAll("^.|.$", "");
        System.out.println(string);

        System.out.println("*************************");
        str = "12345";
        //判断str字符串中是否全部有数字组成，即有1-n个数字组成
        boolean matches = str.matches("\\d+");
        System.out.println(matches);
        String tel = "021-4534289";
        //判断这是否是一个上海的固定电话
        boolean result = tel.matches("021-\\d{7,8}");
        System.out.println(result);

        str = "www|ylaihui|com";
        String[] strs = str.split("\\|");
        for (int i = 0; i < strs.length; i++) {
            System.out.println(strs[i]);
        }
        System.out.println();
    }
}
