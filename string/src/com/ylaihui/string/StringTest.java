//StringTest.java
package com.ylaihui.string;

import org.junit.Test;

public class StringTest {
    @Test
    public void test1() {
        // 方法区中不会存放字符串内容相同的两个字符串的
        // 字面量字符串 "yuanlaihui"，存放在方法区的字符串常量池中， 区别于 new 的方式
        String s1 = "ylaihui";
        String s2 = "ylaihui";

        System.out.println(s1 == s2);
    }

    @Test
    public void test2() {
        String s1 = "ylaihui";
        String s2 = "ylaihui";
        s2 = "lisimmy";
        // 对 s2 重新赋值后， 并没有修改原来方法区中的字符串常量池内容
        // 字符串 lisimmy 是新开辟的一块区域
        System.out.println(s1 == s2);
    }

    @Test
    public void test3() {
        String s1 = "ylaihui";
        String s2 = "ylaihui";
        // 当对现有的字符串进行连接操作时，也需要重新指定内存区域赋值，不能使用原有的value进行赋值。
        s2 += ".com";

        System.out.println(s2); // ylaihui.com
        System.out.println(s1); // ylaihui
    }

    @Test
    public void test4() {
        // 当调用String的replace()方法修改指定字符或字符串时，会重新在指定内存区域赋值，不使用原有的value进行赋值。
        String s1 = "ylaihui";
        String s2 = s1.replace('y', 'Y');

        System.out.println(s1);
        System.out.println(s2);
    }

class Person {
    String name;
    int age;
    Person(String name, int age){
        this.name = name;
        this.age = age;
    }
}

    @Test
    public void test5() {
        String s1 = "lisimmy";
        String s2 = "lisimmy";
        String s3 = new String("lisimmy");
        String s4 = new String("lisimmy");

        System.out.println(s1 == s2); // true
        System.out.println(s1 == s3); // false
        System.out.println(s1 == s4); // false
        System.out.println(s3 == s4); // false

        Person p1 = new Person("lisimmy", 30);
        Person p2 = new Person("lisimmy", 30);

        System.out.println(p1.name.equals(p2.name));  // true
        System.out.println(p1.name == p2.name);  // true 方法区中的字符串常量池中只有一份 "lisimmy"
    }
}
