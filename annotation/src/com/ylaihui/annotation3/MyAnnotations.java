package com.ylaihui.annotation3;

import java.lang.annotation.Repeatable;

public @interface MyAnnotations {
    MyAnn[] value();
}
