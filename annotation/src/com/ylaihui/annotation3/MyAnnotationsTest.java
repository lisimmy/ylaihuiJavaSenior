//MyAnnotationsTest.java
package com.ylaihui.annotation3;

// JDK1.8 新特性，注解可重复，但是需要配置，以下两种写法等价
@MyAnn("yyyy")
@MyAnn("lll")
//@MyAnnotations({@MyAnn("yyyy"), @MyAnn("llll")})
public class MyAnnotationsTest {
}
