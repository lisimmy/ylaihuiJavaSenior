package com.ylaihui.annotation3;

import java.lang.annotation.Repeatable;

@Repeatable(MyAnnotations.class)
public @interface MyAnn {
    String value() default "ylaihui";
}
