//AnnotationTest.java
package com.ylaihui.annotations;

import java.util.ArrayList;
import java.util.Date;

class Order{
    String name;

    public Order() {
    }

    public Order(String name) {
        this.name = name;
    }

    public void show(){
        System.out.println("Order show()...");
    }
}

class YlaihuiOrder extends Order{
    //@Override
    public void shows() {
        System.out.println("YlaihuiOrder show()...");
    }
}

public class AnnotationTest {
    public static void main(String[] args) {
        YlaihuiOrder ylaihuiOrder = new YlaihuiOrder();
        // 本意是想在 YlaihuiOrder 类中重写 Order 类中的 show方法，但是show写为shows了
        // 如果没有使用注解，那么不会报错，也不会有任何提示，容易犯错
        // 有了注解以后， 在shows方法前提供 @Override， 那么会强制检查方法是否真正的重写了父类中的方法
        ylaihuiOrder.show();  // Order show()...

        // Deprecated 已过时的
        Date date = new Date(2020, 10, 11);
        System.out.println(date);

        @SuppressWarnings("unused")
        int num = 10;

        @SuppressWarnings({ "unused", "rawtypes" })
        ArrayList list = new ArrayList();

    }
}
