//MyAnnotationsTest.java
package com.ylaihui.annotation2;

// 注解不能重复
//Duplicate annotation. The declaration of 'com.ylaihui.annotation2.MyAnn' does not have a valid
//@MyAnn("yyyy")
//@MyAnn("lll")
@MyAnnotations({@MyAnn("yyyy"), @MyAnn("llll")})
public class MyAnnotationsTest {
}
