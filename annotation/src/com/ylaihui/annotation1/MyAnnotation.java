package com.ylaihui.annotation1;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;

@Documented  // 所修饰的注解 MyAnnotation 在被javadoc解析时，保留下来
@Retention(RetentionPolicy.RUNTIME)  // MyAnnotation 注解信息 保留到运行时，反射可以获取注解信息
@Target({TYPE, FIELD})   // MyAnnotation 注解可以修饰 类型、属性
@Inherited  // 被它修饰 MyAnnotation 将具有继承性，父类是 MyAnnotation，其子类也是 MyAnnotation
public @interface MyAnnotation {
    String[] value() default "ylaihui";
}
