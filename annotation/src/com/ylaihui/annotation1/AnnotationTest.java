//AnnotationTest.java
package com.ylaihui.annotation1;

import java.lang.annotation.Annotation;

@MyAnnotation
class Person{
    @MyAnnotation
    String name;
    int age;

    // '@MyAnnotation' not applicable to method
    //@MyAnnotation
    public void show(){
        System.out.println();
    }
}

class Student extends Person{

}

public class AnnotationTest {
    public static void main(String[] args) {
        // 子类 Student 继承了父类 Person的注解 MyAnnotation，因为 MyAnnotation 具有继承性是声明为 Inherited 的
        Annotation[] annotations = Student.class.getAnnotations();
        for (int i = 0; i < annotations.length; i++) {
            System.out.println(annotations[i]);
        }
    }
}
