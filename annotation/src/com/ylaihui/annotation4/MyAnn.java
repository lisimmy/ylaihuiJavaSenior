package com.ylaihui.annotation4;

import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.*;

@Target({TYPE_PARAMETER, TYPE_USE})
public @interface MyAnn {
    String[] value() default "ylaihui";
}
