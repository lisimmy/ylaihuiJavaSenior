//StreamAPITest.java
package com.ylaihui.streamapi;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamAPITest {
    //创建 Stream方式一：通过集合
    @Test
    public void test1() {
        List<Integer> integers = Arrays.asList(1, 2, 3, 4, 5);

        //default Stream<E> stream() : 返回一个顺序流
        Stream<Integer> stream = integers.stream();

        //default Stream<E> parallelStream() : 返回一个并行流
        Stream<Integer> parallelStream = integers.parallelStream();

    }

    //创建 Stream方式二：通过数组
    @Test
    public void test2() {
        int[] arr = new int[]{1, 2, 3, 4, 5, 6};
        //调用Arrays类的static <T> Stream<T> stream(T[] array): 返回一个流
        IntStream stream = Arrays.stream(arr);
    }

    //创建 Stream方式三：通过Stream的of()
    @Test
    public void test3() {
        Stream<Integer> stream = Stream.of(1, 2, 3, 4, 5, 6);
    }

    //创建 Stream方式四：创建无限流，用的比较少
    @Test
    public void test4() {
        //迭代
        //public static<T> Stream<T> iterate(final T seed, final UnaryOperator<T> f)
        Stream<Integer> iterate = Stream.iterate(0, t -> t + 2);
        //Stream.iterate(0, t -> t + 2).limit(10).forEach(System.out::println);

        //生成
        //public static<T> Stream<T> generate(Supplier<T> s)
        Stream<Double> generate = Stream.generate(Math::random);
        //Stream.generate(Math::random).limit(10).forEach(System.out::println);
    }
}
