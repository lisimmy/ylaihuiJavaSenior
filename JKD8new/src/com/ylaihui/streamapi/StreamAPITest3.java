//StreamAPITest3.java
package com.ylaihui.streamapi;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

public class StreamAPITest3 {

    //1-匹配与查找
    @Test
    public void test1() {
        List<Integer> list = Arrays.asList(12, 11, 24, 33, 74, 34);

        //allMatch(Predicate p)——检查是否匹配所有元素。
        //检查是否全都大于10
        boolean b = list.stream().allMatch(l -> l > 10);
        System.out.println(b);  // true

        //anyMatch(Predicate p)——检查是否至少匹配一个元素。
        // 检查是否存在大于 70 和 80 的元素
        boolean b1 = list.stream().anyMatch(l -> l > 70);
        System.out.println(b1); // true
        System.out.println(list.stream().anyMatch(l -> l > 80)); // false

        // noneMatch(Predicate p)——检查是否没有匹配的元素。
        // 检查是否没有大于 80 的元素
        boolean b2 = list.stream().noneMatch(l -> l > 80);
        System.out.println(b2);  // true

        // findFirst——返回第一个元素
        Optional<Integer> first = list.stream().findFirst();
        System.out.println(first);  // Optional[12]

        // findAny——返回当前流中的任意元素
        Optional<Integer> any = list.stream().findAny();
        System.out.println(any);  // Optional[12]

        // count——返回流中元素的总个数
        long count = list.stream().count();
        System.out.println(count); // 6

        //max(Comparator c)——返回流中最大值
        Optional<Integer> max = list.stream().max(Integer::compare);
        System.out.println(max); // Optional[74]

        // min(Comparator c)——返回流中最小值
        Optional<Integer> min = list.stream().min(Integer::compare);
        System.out.println(min); // Optional[11]

        // forEach(Consumer c)——内部迭代
        list.stream().forEach(System.out::println);  // Stream 方式的内部迭代遍历数组

        list.forEach(System.out::println);  // Collection 集合中的内部迭代
    }

    //2-归约
    @Test
    public void test2() {
        //reduce(T identity, BinaryOperator)——可以将流中元素反复结合起来，得到一个值。返回 T
        //计算1-10的自然数的和
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
        //0是初始值， 等同于 sum = 0 + sum;
        Integer sum = list.stream().reduce(0, Integer::sum);
        System.out.println(sum);  // Optional[55]

        //reduce(BinaryOperator) ——可以将流中元素反复结合起来，得到一个值。返回 Optional<T>
        Optional<Integer> reduce = list.stream().reduce(Integer::sum);
        Optional<Integer> reduce1 = list.stream().reduce((i1, i2) -> i1 + i2);
        System.out.println(reduce);  // Optional[55]
        System.out.println(reduce1);  // Optional[55]
    }

    //3-收集
    @Test
    public void test3() {
        //collect(Collector c)——将流转换为其他形式。接收一个 Collector接口的实现，用于给Stream中元素做汇总的方法
        List<Integer> list = Arrays.asList(12, 11, 24, 33, 74, 34);

        // 大于 20 的元素返回一个 list
        List<Integer> collect = list.stream().filter(i -> i > 20).collect(Collectors.toList());
        collect.stream().forEach(System.out::println);

        // 大于 20 的元素返回一个 set
        System.out.println();
        Set<Integer> collect1 = list.stream().filter(i -> i > 20).collect(Collectors.toSet());
        collect1.stream().forEach(System.out::println);
    }
}

