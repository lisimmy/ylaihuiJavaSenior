//StreamAPITest2.java
package com.ylaihui.streamapi;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class StreamAPITest2 {
    //1-筛选与切片
    @Test
    public void test1(){
        List<Integer> list = Arrays.asList(1,2,3,1,1,2,3,4,5);
        //filter(Predicate p)——接收 Lambda ， 从流中排除某些元素。
        Stream<Integer> stream = list.stream();
        //查询大于3的
        stream.filter(e -> e==3).forEach(System.out::println);

        System.out.println();
        //limit(n)——截断流，使其元素不超过给定数量。
        list.stream().limit(3).forEach(System.out::println);
        System.out.println();

        //skip(n) —— 跳过元素，返回一个扔掉了前 n 个元素的流。若流中元素不足 n 个，则返回一个空流。与 limit(n) 互补
        list.stream().skip(3).forEach(System.out::println);

        System.out.println();
        //distinct()——筛选，通过流所生成元素的 hashCode() 和 equals() 去除重复元素
        list.stream().distinct().forEach(System.out::println);
    }

    //映射 类型 x -> X  "AA" -> "aa"
    @Test
    public void test2(){
        System.out.println("#1 test..........");
        //map(Function f)——接收一个函数作为参数，将元素转换成其他形式或提取信息，该函数会被应用到每个元素上，并将其映射成一个新的元素。
        List<Integer> list = Arrays.asList(1,2,3,4,5,6,7);
        list.stream().map(i -> i + 1).forEach(System.out::println);


        System.out.println("#2 test..........");
        //获取+1之后 > 5 的结合
        Stream<Integer> stream = list.stream().map(i -> i + 1);
        stream.filter(i -> i > 5).forEach(System.out::println);
        System.out.println();

        System.out.println("#3 test..........");
        List<String> list1 = Arrays.asList("aa", "bb", "cc", "dd");
        Stream<Stream<Character>> streamStream = list1.stream().map(StreamAPITest2::fromStringToStream);
        streamStream.forEach(s ->{
            s.forEach(System.out::println);
        });

        System.out.println("#4test..........");
        //flatMap(Function f)——接收一个函数作为参数，将流中的每个值都换成另一个流，然后把所有流连接成一个流。
        Stream<Character> characterStream = list1.stream().flatMap(StreamAPITest2::fromStringToStream);
        characterStream.forEach(System.out::println);
    }

    //将字符串中的多个字符构成的集合转换为对应的Stream的实例
    public static Stream<Character> fromStringToStream(String str){//aa
        ArrayList<Character> list = new ArrayList<>();
        for(Character c : str.toCharArray()){
            list.add(c);
        }
        return list.stream();
    }

    //3-排序
    @Test
    public void test4(){
        //sorted()——自然排序
        System.out.println("1 test..........");
        List<Integer> list = Arrays.asList(-11, -22, 1, 8 , 2, 88, 22);
        list.stream().sorted().forEach(System.out::println);

        //sorted(Comparator com)——定制排序，由大到小排序
        System.out.println("2 test..........");
        List<Integer> list1 = Arrays.asList(-11, -22, 1, 8 , 2, 88, 22);
        list1.stream().sorted( (l1,l2) -> {

            return -Integer.compare(l1,l2);

        }).forEach(System.out::println);
    }
}
