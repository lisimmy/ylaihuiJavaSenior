package com.ylaihui.methodref;

import org.junit.Test;

import java.util.Arrays;
import java.util.function.Function;
import java.util.function.Supplier;

public class ConstructorRefTest {
    @Test
    public void test1() {
        //普通写法
        Supplier<String> sup = new Supplier<String>() {
            @Override
            public String get() {
                return new String();
            }
        };
        System.out.println(sup.get());

        Supplier<String> sup1 = () -> new String();
        System.out.println(sup1.get());

        //Supplier中的T get()
        //String 的空参构造器：Employee()
        //构造器引用
        Supplier<String> sup2 = String::new;
        System.out.println(sup2.get());

    }


    @Test
    public void test2() {
        Function<String, String> func1 = str -> new String(str);
        String retstr = func1.apply("ylaihui");
        System.out.println(retstr);

        //Function中的R apply(T t)
        //BiFunction中的R apply(T t,U u) 类似， 构造器是需要传入两个参数的情况
        Function<String, String> func2 = String::new;
        String retstr1 = func2.apply("ylaihui");
        System.out.println(retstr1);

    }


    @Test
    public void test4() {
        Function<Integer, String[]> func1 = length -> new String[length];
        String[] arr1 = func1.apply(5);
        System.out.println(Arrays.toString(arr1));

        //数组引用，数组可以看做是一个特殊的类，与new对象一致。
        //Function中的R apply(T t)
        Function<Integer, String[]> func2 = String[]::new;
        String[] arr2 = func2.apply(5);
        System.out.println(Arrays.toString(arr2));
    }
}
