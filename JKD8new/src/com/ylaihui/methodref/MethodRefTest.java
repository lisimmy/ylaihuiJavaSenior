package com.ylaihui.methodref;

import org.junit.Test;
import java.io.PrintStream;
import java.util.Comparator;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public class MethodRefTest {
	@Test
	public void test1() {
		Consumer<String> con1 = str -> System.out.println(str);
		con1.accept("ylaihui");

		//分析下上面的 Lambda表达式
		//Consumer中的抽象方法 void accept(T t)
		//PrintStream中的void println(T t)
		// 将Lambda替换为方法引用： 对象 :: 实例方法

		PrintStream ps = System.out;
		Consumer<String> con2 = ps::println;
		con2.accept("ylaihui");
	}
	

	@Test
	public void test2() {
		String string = new String("ylaihui");

		Supplier<String> sup1 = () -> string.toUpperCase();
		System.out.println(sup1.get());

		//Supplier中的T get()
		//String toUpperCase()
		//将Lambda替换为方法引用

		Supplier<String> sup2 = string::toUpperCase;
		System.out.println(sup2.get());
	}

	@Test
	public void test3() {
		Comparator<Integer> com1 = (t1,t2) -> Integer.compare(t1,t2);
		System.out.println(com1.compare(12,21));

		//Comparator中的int compare(T t1,T t2)
		//Integer中的int compare(T t1,T t2)
		//类 :: 静态方法
		Comparator<Integer> com2 = Integer::compare;
		System.out.println(com2.compare(12,3));

	}
	

	@Test
	public void test4() {
		// 基本做法
		Function<Double,Long> func = new Function<Double, Long>() {
			@Override
			public Long apply(Double d) {
				return Math.round(d);
			}
		};
		System.out.println(func.apply(100.9)); // 101

		//使用 Lambda表达式
		Function<Double,Long> func1 = d -> Math.round(d);
		System.out.println(func1.apply(100.9)); // 101

		//Function中的 R apply(T t)
		//Math中的 Long round(Double d)
		//方法引用
		Function<Double,Long> func2 = Math::round;
		System.out.println(func2.apply(100.9)); // 101
	}

	@Test
	public void test5() {
		Comparator<String> com1 = (s1,s2) -> s1.compareTo(s2);
		System.out.println(com1.compare("ylaihui","ylaihui"));

		// Comparator中的int comapre(T t1,T t2)
		// String中的int t1.compareTo(t2)
		// 类 :: 实例方法，comapre(T t1,T t2)第一个参数t1作为调用者，调用compareTo，该方法的传入参数为t2
		Comparator<String> com2 = String :: compareTo;
		System.out.println(com2.compare("ylaihui","ylaihui"));
	}

	@Test
	public void test6() {
		BiPredicate<String,String> pre1 = (s1,s2) -> s1.equals(s2);
		System.out.println(pre1.test("ylaihui","ylaihui"));

		//BiPredicate中的boolean test(T t1, T t2);
		//String中的boolean t1.equals(t2)
		// 类 :: 实例方法
		BiPredicate<String,String> pre2 = String :: equals;
		System.out.println(pre2.test("ylaihui","ylaihui"));
	}
	

	@Test
	public void test7() {
		String string = new String("ylaihui");
		string.toUpperCase();

		Function<String,String> func1 = str -> str.toUpperCase();
		System.out.println(func1.apply(string));

		// Function中的 R apply(T t)
		// String中的 String toUpperCase()
		// 类 :: 实例方法
		Function<String,String> func2 = String::toUpperCase;
		System.out.println(func2.apply(string));
	}
}
