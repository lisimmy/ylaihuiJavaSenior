//LambdaSyntax1.java
package com.ylaihui.jdk8new;

import org.junit.Test;

public class LambdaSyntax1 {
    // lambda 语法格式一：无参，无返回值
    @Test
    public void test1(){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("no args, no return");
            }
        };

        runnable.run(); // no args, no return

        Runnable runnable1 = () -> System.out.println("no args, no return");
        runnable1.run(); // no args, no return
    }
}
