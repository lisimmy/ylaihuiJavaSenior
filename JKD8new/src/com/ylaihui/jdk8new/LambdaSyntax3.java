//LambdaSyntax3.java
package com.ylaihui.jdk8new;

import org.junit.Test;

import java.util.function.Consumer;

public class LambdaSyntax3 {
    //语法格式三：数据类型可以省略（类型推断）
    @Test
    public void test(){
        Consumer<String> stringConsumer = new Consumer<String>() {
            @Override
            public void accept(String string) {
                System.out.println(string);
            }
        };
        stringConsumer.accept("one arg, no return");  // one arg, no return

        // 使用Lambda表达式替换，省略了参数的类型，类型推断
        Consumer<String> stringConsumer1 = (s) -> System.out.println(s);
        stringConsumer1.accept("one arg, no return");  // one arg, no return
    }
}
