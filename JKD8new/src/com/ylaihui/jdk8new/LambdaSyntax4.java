//LambdaSyntax4.java
package com.ylaihui.jdk8new;

import org.junit.Test;

import java.util.function.Consumer;

public class LambdaSyntax4 {
    //Lambda语法格式四： 只有一个参数时，参数的小括号可以省略
    @Test
    public void test(){
        Consumer<String> stringConsumer = new Consumer<String>() {
            @Override
            public void accept(String string) {
                System.out.println(string);
            }
        };
        stringConsumer.accept("one arg, no return");  // one arg, no return

        // 使用Lambda表达式替换，只有一个参数，小括号（） 省略
        Consumer<String> stringConsumer1 = s -> System.out.println(s);
        stringConsumer1.accept("one arg, no return");  // one arg, no return
    }
}
