//LambdaSyntax6.java
package com.ylaihui.jdk8new;

import org.junit.Test;

import java.util.Comparator;

public class LambdaSyntax6 {
    //Lambda 语法格式六：方法体只有一条语句时，return 与大括号若有，都可以省略
    @Test
    public void test(){
        Comparator<Integer> integerComparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        };

        System.out.println(integerComparator.compare(1, 2));  // -1

        // 使用 Lambda 替换以上写法 ， 只有一条语句， return 和 {} 都省略
        //Comparator<Integer> integerComparator1 = (o1,o2) -> {
        //    return o1.compareTo(o2);
        //};
        Comparator<Integer> integerComparator1 = (o1,o2) -> o1.compareTo(o2);
        System.out.println(integerComparator1.compare(1, 2));  // -1
    }
}
