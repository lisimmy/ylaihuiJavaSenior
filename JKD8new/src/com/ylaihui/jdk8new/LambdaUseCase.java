//LambdaUseCase.java
package com.ylaihui.jdk8new;

import org.junit.Test;

import java.util.Comparator;

public class LambdaUseCase {
    @Test
    public void test1(){
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                System.out.println("Thread run method...");
            }
        };
        runnable.run();  // Thread run method...

        //将以上改为使用 Lambda表达式
        Runnable runnable1 = () -> System.out.println("Thread run method...");
        runnable1.run();  // Thread run method...
    }

    @Test
    public void test2(){
        Comparator<Integer> comparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1.compareTo(o2);
            }
        };

        System.out.println(comparator.compare(1, 2)); // -1

        //将以上改为使用 Lambda表达式
        Comparator<Integer> comparator1 =(o1, o2) -> o1.compareTo(o2);

        System.out.println(comparator1.compare(1, 2)); // -1
    }
}
