//FunctionInterfaceTest.java
package com.ylaihui.jdk8new;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class FunctionInterfaceTest {

    public void func(Double arg, Consumer<Double> com){
        com.accept(arg);
    }

    @Test
    public void test(){
        func( 128.00, new Consumer<Double>() {
            @Override
            public void accept(Double aDouble) {
                System.out.println("消费金额" + aDouble);
            }
        });  // 消费金额128.0

        //使用 Lambda 表达式
        func(128.00, (aDouble) -> System.out.println("消费金额" + aDouble));  // 消费金额128.0
    }

    // 函数式接口Predicate， 具体的规则是由传入的接口实现类的对象（Lambda表达式）决定的
    public List<String> filterString(List<String> list, Predicate<String> predicate){
        ArrayList<String> ts = new ArrayList<String>();

        for (String t : list) {
            if(predicate.test(t))
                ts.add(t);
        }
        return ts;
    }

    @Test
    public void test2(){
        List<String> strings = Arrays.asList("101", "111", "1001", "1000", "11221");

        // 简单理解： str 是 接口中方法需要传入的参数， str.contains("11") 是需要实现的方法的方法体
        List<String> strs = filterString(strings, str -> str.contains("11") );

        System.out.println(strs);  // [111, 11221]
    }
}
