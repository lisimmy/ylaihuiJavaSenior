//LambdaSyntax5.java
package com.ylaihui.jdk8new;

import org.junit.Test;

import java.util.Comparator;

public class LambdaSyntax5 {
    //Lambda 语法格式五： 需要两个或以上的参数，多条执行语句，并且可以有返回值
    @Test
    public void test(){
        Comparator<Integer> integerComparator = new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                System.out.println("arg1:" + o1);
                System.out.println("arg2:" + o2);
                return o1.compareTo(o2);
            }
        };

        System.out.println(integerComparator.compare(1, 2));  // -1

        // 使用 Lambda 替换以上写法 ， 有返回值，有两个参数的情况
        Comparator<Integer> integerComparator1 = (o1,o2) -> {
            System.out.println("arg1:" + o1);
            System.out.println("arg2:" + o2);
            return o1.compareTo(o2);
        };
        System.out.println(integerComparator1.compare(1, 2));  // -1
    }
}
