//LambdaSyntax2.java
package com.ylaihui.jdk8new;

import org.junit.Test;

import java.util.function.Consumer;

public class LambdaSyntax2 {
    //Lambda 语法格式二： 需要一个参数，没有返回值
    @Test
    public void test(){
        Consumer<String> stringConsumer = new Consumer<String>() {
            @Override
            public void accept(String string) {
                System.out.println(string);
            }
        };
        stringConsumer.accept("one arg, no return");  // one arg, no return

        // 使用Lambda表达式替换
        Consumer<String> stringConsumer1 = (String s) -> System.out.println(s);
        stringConsumer1.accept("one arg, no return");  // one arg, no return

    }
}
