//Animal.java
package com.ylaihui.comparable;

public class Animal implements Comparable{
    String name;
    int age;

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public Animal() {
    }

    @Override
    public String toString() {
        return "Animal{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        if(o instanceof Animal){
            Animal animal = (Animal)o;
            if(this.age > animal.age)
                return 1;
            else if(this.age < animal.age)
                return -1;
            else
                return 0;
        }else
            throw new RuntimeException("类型不一致，无法比较");
    }
}
