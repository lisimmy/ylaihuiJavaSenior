//AnimalSort.java
package com.ylaihui.comparable;

import java.util.Arrays;

public class AnimalSort {
    public static void main(String[] args) {
        Animal[] animals= new Animal[4];
        animals[0] = new Animal("yy",22);
        animals[1] = new Animal("ll",23);
        animals[2] = new Animal("aa",24);
        animals[3] = new Animal("ii",25);

        Arrays.sort(animals);
        System.out.println("sort by age");
        System.out.println(Arrays.toString(animals));
    }
}
