//PersonSort.java
package com.ylaihui.comparable;

import java.util.Arrays;

public class PersonSort {
    public static void main(String[] args) {
        Person[] people = new Person[4];
        people[0] = new Person("yy",22);
        people[1] = new Person("ll",23);
        people[2] = new Person("aa",24);
        people[3] = new Person("ii",25);

        // ClassCastException: com.ylaihui.comparable.Person cannot be cast to java.lang.Comparable
        Arrays.sort(people);

        System.out.println(people);
    }
}
