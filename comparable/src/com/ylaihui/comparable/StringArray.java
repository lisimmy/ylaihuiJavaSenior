//StringArray.java
package com.ylaihui.comparable;
import org.junit.Test;
import java.util.Arrays;

public class StringArray {
    @Test
    public void test1(){
        String[] strs = new String[]{"yy","ll","aa","ii","hh","uu","ii"};

        Arrays.sort(strs);

        System.out.println(Arrays.toString(strs));  // [aa, hh, ii, ii, ll, uu, yy]
    }
}
