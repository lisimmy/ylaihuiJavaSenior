//EnumStatus.java
package com.ylaihui.enumabstract;

interface Desc{
    public void show();
}

enum Status implements Desc{
    VALID("valid"),
    NONVALID("nonvalid");

    private final String name;

    private Status(String name) {
        this.name = name;
    }

    @Override
    public void show() {
        System.out.println("enum Status: " + this.name);
    }
}

public class EnumStatus {
    public static void main(String[] args) {
        Status[] values = Status.values();
        for (int i = 0; i < values.length; i++) {
            System.out.println(values[i]);
            values[i].show();
        }
    }
}
