//EnumStatus.java
package com.ylaihui.enumabstractmethod;

interface Desc{
    public void show();
}

enum Status implements Desc{
    VALID("valid"){
        @Override
        public void show() {
            System.out.println("in object show : valid");
        }
    },
    NONVALID("nonvalid"){
        @Override
        public void show() {
            System.out.println("in object show : nonvalid");
        }
    };

    private final String name;

    private Status(String name) {
        this.name = name;
    }
}

public class EnumStatus {
    public static void main(String[] args) {
        Status[] values = Status.values();
        for (int i = 0; i < values.length; i++) {
            System.out.println(values[i]);
            values[i].show();
        }
    }
}
