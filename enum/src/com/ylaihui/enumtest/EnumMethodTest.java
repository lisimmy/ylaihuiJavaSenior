//EnumMethodTest.java
package com.ylaihui.enumtest;

import java.util.Arrays;

enum EnumStatus1{
    VALID("valid"),
    NONVALID("nonvalid");

    private final String name;

    private EnumStatus1(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    //@Override
    //public String toString() {
    //    return "EnumStatus{" +
    //            "name='" + name + '\'' +
    //            '}';
    //}
}
public class EnumMethodTest {
    public static void main(String[] args) {
        //values() 方法
        EnumStatus1[] values = EnumStatus1.values();
        System.out.println(Arrays.toString(values));

        for (int i = 0; i <values.length; i++) {
            System.out.println(values[i]);
        }

        Thread.State[] values1 = Thread.State.values();
        for (int i = 0; i < values1.length; i++) {
            System.out.println("Thread State: " + values1[i]);
        }

        //valueOf(String objName):返回枚举类中对象名是objName的对象。
        Thread.State state = Thread.State.valueOf("NEW");
        System.out.println(state);
        //IllegalArgumentException: No enum constant java.lang.Thread.State.DDD
        //Thread.State state1 = Thread.State.valueOf("DDD");
        //System.out.println(state1);

        //toString()：返回当前枚举类对象常量的名称
        String s = state.toString();
        System.out.println(s);
    }
}
