//StatusTest.java
package com.ylaihui.enumtest;

class Status{
    private final String name;

    private Status(String name){
        this.name = name;
    }

    public static final Status VALID = new Status("valid");
    public static final Status NONVALID = new Status("nonvalid");

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Status{" +
                "name='" + name + '\'' +
                '}';
    }
}

public class StatusTest {
    public static void main(String[] args) {
        Status valid = Status.VALID;
        System.out.println(valid);

        Status nonvalid = Status.NONVALID;
        System.out.println(nonvalid);
    }
}
