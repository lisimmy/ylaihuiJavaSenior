//EnumStatusTest.java
package com.ylaihui.enumtest;

enum EnumStatus{
    VALID("valid"),
    NONVALID("nonvalid");

    private final String name;

    private EnumStatus(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    //@Override
    //public String toString() {
    //    return "EnumStatus{" +
    //            "name='" + name + '\'' +
    //            '}';
    //}
}

public class EnumStatusTest {
    public static void main(String[] args) {
        EnumStatus valid = EnumStatus.VALID;
        EnumStatus nonvalid = EnumStatus.NONVALID;
        System.out.println(valid); // VALID
        System.out.println(nonvalid); // NONVALID
    }
}
