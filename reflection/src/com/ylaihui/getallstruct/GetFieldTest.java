//GetFieldTest.java
package com.ylaihui.getallstruct;

import org.junit.Test;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

//获取当前运行时类的属性结构
public class GetFieldTest {
    @Test
    public void getfiled() throws ClassNotFoundException {
        Class<?> clazz = Class.forName("com.ylaihui.getallstruct.Person");

        //获取属性结构
        //getFields():获取当前运行时类及其父类中声明为 public 访问权限的属性
        Field[] fields = clazz.getFields();
        for (Field field : fields) {
            System.out.println(field);
        }

        //getDeclaredFields():获取当前运行时类中声明的所有属性。（不包含父类中声明的属性）
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            System.out.println(declaredField);
        }
    }

    //获取权限修饰符  数据类型 变量名
    @Test
    public void getfiledall() throws ClassNotFoundException {
        Class<?> clazz = Class.forName("com.ylaihui.getallstruct.Person");
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            // 权限修饰符
            int modifiers = field.getModifiers();
            System.out.print(Modifier.toString(modifiers) + " ");

            // 获取数据类型
            Class<?> type = field.getType();
            System.out.print(type.getName() + " ");

            // 获取属性名
            String name = field.getName();
            System.out.println(name + ";");

            System.out.println();
        }
    }

}

