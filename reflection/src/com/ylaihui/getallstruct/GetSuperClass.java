//GetSuperClass.java
package com.ylaihui.getallstruct;


import org.junit.Test;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public class GetSuperClass {
    @Test
    public void test(){
        // 获取父类
        Class<Person> clazz = Person.class;
        Class<? super Person> superclass = clazz.getSuperclass();

        System.out.println(superclass);  //class com.ylaihui.getallstruct.Creature

        //获取带泛型的父类
        Type type = clazz.getGenericSuperclass();
        System.out.println(type);  // com.ylaihui.getallstruct.Creature<java.lang.String>

        //获取带泛型的父类 的 泛型参数（泛型类型）
        ParameterizedType ptype = (ParameterizedType) type;
        Type[] actualTypeArguments = ((ParameterizedType) type).getActualTypeArguments();
        for (Type actualTypeArgument : actualTypeArguments) {
            System.out.println(actualTypeArgument);  // class java.lang.String
            System.out.println(actualTypeArgument.getTypeName()); // java.lang.String
        }
    }
}
