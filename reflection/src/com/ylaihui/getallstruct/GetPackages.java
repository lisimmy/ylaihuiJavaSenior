//GetPackages.java
package com.ylaihui.getallstruct;

import org.junit.Test;

import java.lang.annotation.Annotation;

public class GetPackages {
    @Test
    public void test() throws ClassNotFoundException {
        // 获取运行时类所在的包
        Class<?> aClass = Class.forName("com.ylaihui.getallstruct.Person");
        Package aPackage = aClass.getPackage();

        System.out.println(aPackage);  // package com.ylaihui.getallstruct

        //获取运行时类的注解
        ClassLoader classLoader = GetPackages.class.getClassLoader();
        Class<?> aClass1 = classLoader.loadClass("com.ylaihui.getallstruct.Person");

        Annotation[] annotations = aClass1.getAnnotations();
        for (Annotation annotation : annotations) {
            System.out.println(annotation);  // @com.ylaihui.getallstruct.MyAnnotation(value=hi)
        }
    }
}
