//GetAdnSetFields.java
package com.ylaihui.getallstruct;

import org.junit.Test;

import java.lang.reflect.Field;

public class GetAdnSetFields {
    @Test
    public void test() throws Exception {
        Class<?> clazz = Class.forName("com.ylaihui.getallstruct.Person");

        Person p = (Person) clazz.newInstance();

        // getField() 获取运行时类中属性声明为public的 id 属性
        // getDeclaredField() 是获取类中所有的属性
        Field id = clazz.getField("id");

        id.set(p, 101);

        int pid = (int) id.get(p);
        System.out.println(pid);
    }

    @Test
    public void test1() throws Exception {
        Class<?> clazz = Class.forName("com.ylaihui.getallstruct.Person");

        Person p = (Person) clazz.newInstance();

        // getDeclaredField() 是获取类中所有的属性
        Field id = clazz.getDeclaredField("id");

        id.set(p, 101);

        int pid = (int) id.get(p);
        System.out.println(pid);
    }
}
