//GetInterfaces.java
package com.ylaihui.getallstruct;

import org.junit.Test;

public class GetInterfaces {
    @Test
    public void test(){
        // 获取运行时类的实现的接口
        Class<Person> clazz = Person.class;

        Class<?>[] interfaces = clazz.getInterfaces();
        for (Class<?> anInterface : interfaces) {
            System.out.println(anInterface);
        }

        // 运行时类的父类 实现的接口
        Class<?>[] interfaces1 = clazz.getSuperclass().getInterfaces();
        for (Class<?> aClass : interfaces1) {
            System.out.println(aClass);
        }
    }
}
