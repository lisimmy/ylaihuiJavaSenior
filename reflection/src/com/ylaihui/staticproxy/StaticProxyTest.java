//StaticProxyTest.java
package com.ylaihui.staticproxy;

interface Productor{
    public void make();
}

//代理类
class ProxyPhone implements Productor{
    public Productor pro;
    ProxyPhone(Productor pro){
        this.pro = pro;
    }
    @Override
    public void make() {
        System.out.println("before make logic proc!");
        pro.make();  // 被代理类去make
        System.out.println("end make logic proc!");
    }
}

//被代理类
class ApplePhone implements Productor{
    @Override
    public void make() {
        System.out.println("make phone");
    }
}

public class StaticProxyTest {
    public static void main(String[] args) {
        ProxyPhone proxyPhone = new ProxyPhone(new ApplePhone());
        proxyPhone.make();
    }
}