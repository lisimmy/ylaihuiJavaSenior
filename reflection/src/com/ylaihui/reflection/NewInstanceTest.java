//NewInstanceTest.java
package com.ylaihui.reflection;

import org.junit.Test;

public class NewInstanceTest {
    @Test
    public void test() throws IllegalAccessException, InstantiationException {
        Class<Productor> p = Productor.class;
        Productor productor = p.newInstance();

        System.out.println(productor);  // Productor{name='null', price=0.0}
    }
}
