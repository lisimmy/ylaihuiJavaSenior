//ReflectionNewObj.java
package com.ylaihui.reflection;

import org.junit.Test;

public class ReflectionNewObj {
    @Test
    public void test() throws IllegalAccessException, InstantiationException, ClassNotFoundException {
        // Java程序运行时，可以 根据配置 获取 类的 classPath 全类名，动态的创建类的对象
        // 编译的时候不知道需要创建哪个类的对象
        // 一些框架中大量使用反射，通过配置直接用，就是体现了反射的动态性
        Object instance = getInstance("com.ylaihui.reflection.Productor");

        System.out.println(instance);
    }

    // classPath:指定类的全类名
    public Object getInstance(String classPath) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        Class<?> aClass = Class.forName(classPath);
        return aClass.newInstance();
    }
}
