//ProperitiesTest.java
package com.ylaihui.reflection;

import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesTest {
    @Test
    public void test() throws IOException {
        Properties prp = new Properties();
        FileInputStream fi = new FileInputStream("src\\jdbc.properties");
        prp.load(fi);

        String name = prp.getProperty("name");
        String password = prp.getProperty("password");
        System.out.println(name);
        System.out.println(password);
    }

    @Test
    public void test1() throws IOException {
        Properties prp = new Properties();

        ClassLoader classLoader = PropertiesTest.class.getClassLoader();
        InputStream fi = classLoader.getResourceAsStream("jdbc.properties");
        prp.load(fi);

        String name = prp.getProperty("name");
        String password = prp.getProperty("password");
        System.out.println(name);
        System.out.println(password);
    }
}
