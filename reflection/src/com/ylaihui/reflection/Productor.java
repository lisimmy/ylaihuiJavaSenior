//Productor.java
package com.ylaihui.reflection;

public class Productor {
    private String name;
    public double price;

    public Productor() {
    }

    public Productor(String name, double price) {
        this.name = name;
        this.price = price;
    }

    private Productor(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Productor{" +
                "name='" + name + '\'' +
                ", price=" + price +
                '}';
    }

    private void showInfo() {
        System.out.println(name + ":" + price);
    }

    public String showName() {
        System.out.println(name);
        return name;
    }

    public String showName(String name) {
        System.out.println(name);
        return name;
    }

}
