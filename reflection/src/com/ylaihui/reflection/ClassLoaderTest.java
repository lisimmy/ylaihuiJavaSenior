//ClassLoaderTest.java
package com.ylaihui.reflection;

import org.junit.Test;
public class ClassLoaderTest {
    @Test
    public void test(){
        // 对于自定义类，使用系统类加载器进行加载（ 获取当前自定义类ClassLoaderTest的加载器 ）
        ClassLoader classLoader = ClassLoaderTest.class.getClassLoader();
        System.out.println(classLoader); // sun.misc.Launcher$AppClassLoader@18b4aac2

        // 调用系统类加载器的getParent()：获取扩展类加载器，获取上一层的加载器
        ClassLoader classLoader1 = classLoader.getParent();
        System.out.println(classLoader1);  // sun.misc.Launcher$ExtClassLoader@8efb846

        //调用扩展类加载器的getParent()：无法获取引导类加载器
        //引导类加载器主要负责加载java的核心类库，无法加载自定义类的
        ClassLoader classLoader2 = classLoader1.getParent();
        System.out.println(classLoader2);  // null

        // 通过String类，核心类，获取引导类加载器（无法获取）
        ClassLoader classLoader3 = String.class.getClassLoader();
        System.out.println(classLoader3);  // null
    }
}


