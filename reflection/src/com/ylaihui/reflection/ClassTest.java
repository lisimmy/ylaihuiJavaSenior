//ClassTest.java
package com.ylaihui.reflection;

import org.junit.Test;

public class ClassTest {
    @Test
    public void classtest() throws ClassNotFoundException {
        // 1. Xxx.class
        Class<Productor> clazz1 = Productor.class;
        System.out.println(clazz1);

        // 2. 通过类的对象
        Productor p = new Productor();
        Class<? extends Productor> clazz2 = p.getClass();
        System.out.println(clazz2);

        // 3. 比较常用的方法：通过Class的静态方法forName(全类名)  ---- ClassNotFoundException
        Class<?> clazz3 = Class.forName("com.ylaihui.reflection.Productor");
        System.out.println(clazz3);
        
        // 4.通过ClassLoader
        ClassLoader classLoader = ClassTest.class.getClassLoader();
        Class<?> clazz4 = classLoader.loadClass("com.ylaihui.reflection.Productor");
        System.out.println(clazz4);
    }

}
