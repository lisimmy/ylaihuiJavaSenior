//ClassObjectTest.java
package com.ylaihui.reflection;

import org.junit.Test;

import java.lang.annotation.ElementType;

public class ClassObjectTest {
    @Test
    public void test(){
        System.out.println(Object.class);
        System.out.println(String.class);
        System.out.println(String[].class);
        System.out.println(Comparable.class); // 接口
        System.out.println(int[][].class);
        System.out.println(ElementType.class); // 枚举类
        System.out.println(Override.class);  // 注解
        System.out.println(int.class);
        System.out.println(void.class);
        System.out.println(Class.class);

        int[] arr1 = new int[100];
        int[] arr2 = new int[200];
        System.out.println(arr1.getClass() == arr2.getClass()); // true
    }
}
