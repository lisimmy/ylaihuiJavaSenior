//WildcardSuperExtend.java
package com.ylaihui.generic;

import java.util.ArrayList;
import java.util.List;

class Person {
}
class Student extends Person {
}

public class WildcardSuperExtend {
    public static void main(String[] args) {
        // ? <= Person
        List<? extends Person> list1 = null;
        // ? >= Person
        List<? super Person> list2 = null;

        List<Student> list3 = new ArrayList<Student>();
        List<Person> list4 = new ArrayList<Person>();
        List<Object> list5 = new ArrayList<Object>();

        //理解： List<? extends Person> 可作为 List<Student> 和 List<Person> 的父类
        list1 = list3;
        list1 = list4;
        //list1 = list5;

        //list2 = list3;
        list2 = list4;
        list2 = list5;

        //--------------读取数据：
        list1 = list3;
        // list1 中的对象，都是 Person类或Person子类的对象
        Person p = list1.get(0);
        Object p1 = list1.get(0);
        //编译不通过，可能存在一个类A，是Person的子类，但是是Student的父类
        // 导致父类的对象赋值给子类的引用，错误的。
        //Student s = list1.get(0);

        list2 = list4;
        // 使用顶级父类的引用接收子类对象（多态的体现）
        Object obj = list2.get(0);
        ////编译不通过
        // ? 至少是 一个Person 类， 父类对象无法赋值给子类引用
        //Person obj = list2.get(0);

        //--------------写入数据：
        //list1 不能写入数据， 编译不通过
        // ? 代表 Person 的所有子类，如果 ？表示的类，也是Student的子类
        // 那么 ？ = new Student(); 就不合适， 父类对象赋值给子类的引用了。
        // 只能子类对象赋值给父类引用（多态的体现）
        //list1.add(new Student());

        //编译通过
        list2.add(new Person());
        list2.add(new Student());
    }
}
