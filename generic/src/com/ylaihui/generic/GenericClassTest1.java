//GenericClassTest1.java
package com.ylaihui.generic;

class Phone<T>{
    String name;
    double price;
    T extend;

    public T getExtend() {
        return extend;
    }

    public void setExtend(T extend) {
        this.extend = extend;
    }

    public Phone() {
    }
}

class LenovePhone<T> extends Phone<T>{
    T extend1;

    public T getExtend1() {
        return extend1;
    }

    public void setExtend1(T extend1) {
        this.extend1 = extend1;
    }
}

public class GenericClassTest1 {
    public static void main(String[] args) {
        LenovePhone<String> lp = new LenovePhone<>();
        lp.setExtend("ylaihui");
        lp.setExtend1("ylaihui-Phone");
        System.out.println(lp.getExtend());  // ylaihui
        System.out.println(lp.getExtend1()); // ylaihui-Phone
    }
}

