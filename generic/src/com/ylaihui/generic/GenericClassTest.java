//GenericClassTest.java
package com.ylaihui.generic;

class Book<T>{
    int id;
    String name;
    T extend;

    public T getExtend() {
        return extend;
    }

    public void setExtend(T extend) {
        this.extend = extend;
    }
}

class ITBook extends Book<String>{
    public ITBook() {
    }
}

public class GenericClassTest {
    public static void main(String[] args) {
        ITBook itBook = new ITBook();
        itBook.setExtend("This is ylaihui book!");
        String extend = itBook.getExtend();
        System.out.println(extend);
    }
}
