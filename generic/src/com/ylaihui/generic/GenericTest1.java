//GenericTest1.java
package com.ylaihui.generic;

import org.junit.Test;

import java.util.*;

public class GenericTest1 {

    @Test
    public void test1() {
        ArrayList<Double> ds = new ArrayList<>();
        ds.add(11.11);
        ds.add(22.22);
        // 确定了类型，不需要将Object类型转换其他类型
        Iterator<Double> iterator = ds.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    @Test
    public void test2() {
        HashMap<String, Integer> map = new HashMap<>();
        map.put("ylaihui", 111);
        map.put("ylai", 222);
        // 确定了类型，更加方便、安全的操作集合数据
        Set<Map.Entry<String, Integer>> entries = map.entrySet();
        Iterator<Map.Entry<String, Integer>> iterator = entries.iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Integer> next = iterator.next();
            System.out.println(next.getKey() + ":" + next.getValue());
        }
    }
}
