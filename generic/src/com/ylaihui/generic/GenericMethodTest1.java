//GenericMethodTest1.java
package com.ylaihui.generic;

import java.util.ArrayList;
import java.util.List;

class Computer<T>{
    int id;
    String name;
    T extendInfo;

    public <E> List<E> transFromArrayToList(E[] array){
        ArrayList<E> es = new ArrayList<E>();
        for (int i = 0; i <array.length; i++) {
            es.add(array[i]);
        }
        return es;
    }

    public static <E> List<E> statictransFromArrayToList(E[] array){
        ArrayList<E> es = new ArrayList<E>();
        for (int i = 0; i <array.length; i++) {
            es.add(array[i]);
        }
        return es;
    }
}

public class GenericMethodTest1 {
    public static void main(String[] args) {
        // 实例化泛型类的时候，指明泛型参数为 Double
        Computer<Double> dc = new Computer<>();

        Boolean[] bls = new Boolean[]{true, false, true, false};
        // 调用泛型方法 transFromArrayToList 时，指定了泛型参数 Boolean
        // 意味着 在方法中出现了泛型的结构，方法的泛型参数与类的泛型参数没有任何关系
        System.out.println("#1 generic method test");
        List<Boolean> booleans = dc.transFromArrayToList(bls);
        for (Boolean b : booleans) {
            System.out.println(b);
        }
        System.out.println("#2 static generic method test");
        List<Boolean> bls1 = Computer.statictransFromArrayToList(bls);
        for (Boolean b : bls1) {
            System.out.println(b);
        }

    }
}
