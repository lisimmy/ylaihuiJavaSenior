//GenericTypeTest.java
package com.ylaihui.generic;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class GenericTypeTest {
    public static void main(String[] args) {

        ArrayList<Object> list1 = new ArrayList<>();
        ArrayList<Integer> list2 = new ArrayList<>();

        //类A是类B的父类,G<A> 和G<B>二者不具备子父类关系，二者是并列关系
        //list1 = list2;  // ERROR: Required type:ArrayList<Object>  Provided:ArrayList<Integer>
        // 但是，在运行时只有一个ArrayList被加载到JVM中。
        System.out.println(list1.getClass());
        System.out.println(list2.getClass());

        //类A是类B的父类，A<G> 是 B<G> 的父类
        Map<String, Integer> map1 = new HashMap<String, Integer>();
        Map<String, Integer> map2 = new LinkedHashMap<String, Integer>();

        map1 = map2;
        System.out.println(map1.getClass());
        System.out.println(map2.getClass());
    }
}
