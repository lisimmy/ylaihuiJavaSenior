//ArrayListTest1.java
package com.ylaihui.generic;

import java.util.ArrayList;

public class ArrayListTest1 {
    public static void main(String[] args) {
        ArrayList<String> strs = new ArrayList<>();
        //strs.add(111); //Required type: String Provided: int
        //strs.add(new Date()); // Required type: String Provided:Date
        strs.add("ylaihui");
    }
}