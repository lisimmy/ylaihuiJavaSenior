//GenericWildcard.java
package com.ylaihui.generic;

import java.util.ArrayList;

public class GenericWildcard {

    public static void method(ArrayList<Object> list) {
        for (Object o : list) {
            System.out.println(o);
        }
    }

    private static void methodWild(ArrayList<?> list) {
        // 不管 ArrayList 中的元素类型是什么， 都可以使用 Object 类型接收
        for (Object o : list) {
            System.out.print(o);
        }
        System.out.println();
    }

    public static void main(String[] args) {
        ArrayList<Object> list1 = new ArrayList<>();
        ArrayList<Integer> list2 = new ArrayList<>();
        ArrayList<String> list3 = new ArrayList<>();

        method(list1);
        // 有了泛型以后， 这里的method通用性降低了？
        // 如果想让 method 更加通用怎么办呢？
        //method(list2);  // ERROR
        //method(list3);  // ERROR

        // 泛型中 通配符的使用 将 method改造为 methodWild
        list3.add("www.");
        list3.add("ylaihui");
        list3.add(".com");
        methodWild(list1);
        methodWild(list2);
        methodWild(list3);

        // 添加数据
        ArrayList<?> list = new ArrayList<>();
        list = list3;
        // ？是不确定的，这里就禁止添加数据了
        // 只能添加 null
        //list.add();  // capture of ? e

        // 获取数据
        // 虽然数据类型是不确定的，但是一定是一个对象，可以使用Object类型接收
        Object o = list.get(0);
        System.out.println(o.getClass());
        System.out.println(o); // www.
    }
}