//GenericTest2.java
package com.ylaihui.generic;

class User<T>{
    int id;
    String name;
    T info;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public T getInfo() {
        return info;
    }

    public void setInfo(T info) {
        this.info = info;
    }

    public User(int id, String name, T info) {
        this.id = id;
        this.name = name;
        this.info = info;
    }

    public User() {
    }
}

public class GenericTest2 {
    public static void main(String[] args) {
        User user = new User();
        //如果定义了泛型类，实例化时没有指明类的泛型，则认为此泛型类型为Object类型
        //如果定义了类是带泛型的，建议在实例化时要指明类的泛型。
        user.setInfo(new String("ylaihui"));
        System.out.println(user.getInfo());  // ylaihui

        // 建议按以下方式使用泛型类
        User<String> su = new User<>();
        su.setInfo("aaa");
        System.out.println(su.getInfo()); // aaa

    }
}
