//InetAddressTest.java
package com.ylaihui.net;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class InetAddressTest {
    public static void main(String[] args) {

        try {
            InetAddress addr = InetAddress.getByName("www.ylaihui.com");
            System.out.println(addr.getHostAddress());
            System.out.println(addr.getHostName());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        try {
            InetAddress addr1 = InetAddress.getByName("180.101.49.12");
            System.out.println(addr1.getHostAddress());
            System.out.println(addr1.getHostName());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        try {
            System.out.println(InetAddress.getLocalHost());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

    }
}
