//SimpleDateFormatTest.java
package com.ylaihui.date;

import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import java.util.Date;

public class SimpleDateFormatTest {
    @Test
    public void test1() throws ParseException {
        // SimpleDateFormat 的默认格式化和解析
        SimpleDateFormat sdf1 = new SimpleDateFormat();
        Date date = new Date();
        System.out.println("原日期默认格式：" + date);
        String f1 = sdf1.format(date);
        System.out.println("默认格式化的格式：" + f1);  // YY-MM-DD 上午HH:MM

        Date parse = sdf1.parse("25-1-1 上午1:00");
        System.out.println(parse);
        //Date parse1 = sdf1.parse("25-1-1 1:00"); // ParseException

        // SimpleDateFormat 的指定格式化解析
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        String f2 = sdf2.format(date);
        System.out.println(f2);

        Date parse1 = sdf2.parse("2025-01-01 01:00:00");
        System.out.println(parse1);
    }

    @Test
    public void test2(){
        //1.实例化
        //方式一：创建其子类（GregorianCalendar）的对象
        //方式二：调用其静态方法getInstance()
        Calendar calendar = Calendar.getInstance();
        System.out.println(calendar.getClass());  // 查看 instance实例是哪个类new的 class java.util.GregorianCalendar

        //2.常用方法
        //set()
        //calendar可变性 2025-01-01
        calendar.set(Calendar.YEAR, 2025);
        calendar.set(Calendar.MONTH, Calendar.JANUARY);  // JANUARY == 0
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        //get()
        int days = calendar.get(Calendar.DAY_OF_MONTH); // 1
        System.out.println(days);
        System.out.println(calendar.get(Calendar.YEAR)); // 2025
        System.out.println(calendar.get(Calendar.DAY_OF_MONTH)); // 1

        //add()
        calendar.add(Calendar.DAY_OF_MONTH,-3);
        days = calendar.get(Calendar.DAY_OF_MONTH);
        System.out.println(calendar.get(Calendar.YEAR));// 2024

        System.out.println(days); // 29

        //getTime():日历类---> Date
        Date date = calendar.getTime();
        System.out.println(date);

        //setTime():Date ---> 日历类
        Date date1 = new Date();
        calendar.setTime(date1);
        days = calendar.get(Calendar.DAY_OF_MONTH);
        System.out.println(days);
    }

    @Test
    public void test3(){
        //now():获取当前的日期、时间、日期+时间
        LocalDate localDate = LocalDate.now();
        LocalTime localTime = LocalTime.now();
        LocalDateTime localDateTime = LocalDateTime.now();

        System.out.println(localDate);
        System.out.println(localTime);
        System.out.println(localDateTime);

        //of():设置指定的年、月、日、时、分、秒。没有偏移量
        LocalDateTime localDateTime1 = LocalDateTime.of(2025, 1, 11, 22, 12, 43);
        System.out.println(localDateTime1);


        //getXxx()：获取相关的属性
        System.out.println(localDateTime.getDayOfMonth());
        System.out.println(localDateTime.getDayOfWeek());
        System.out.println(localDateTime.getMonth());
        System.out.println(localDateTime.getMonthValue());
        System.out.println(localDateTime.getMinute());

        //体现不可变性，设置时会产生新的对象
        //withXxx():设置相关的属性
        LocalDate localDate1 = localDate.withDayOfMonth(22);
        System.out.println(localDate);
        System.out.println(localDate1);


        LocalDateTime localDateTime2 = localDateTime.withHour(4);
        System.out.println(localDateTime);
        System.out.println(localDateTime2);

        //不可变性
        LocalDateTime localDateTime3 = localDateTime.plusMonths(3);
        System.out.println(localDateTime);
        System.out.println(localDateTime3);

        LocalDateTime localDateTime4 = localDateTime.minusDays(6);
        System.out.println(localDateTime);
        System.out.println(localDateTime4);
    }

}
