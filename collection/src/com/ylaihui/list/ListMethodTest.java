//ListMethodTest.java
package com.ylaihui.list;

import java.util.ArrayList;
import java.util.List;

public class ListMethodTest {
    public static void main(String[] args) {
        ArrayList list = new ArrayList();
        list.add(100);
        list.add("ylai");
        list.add(200);
        list.add("hui");

        System.out.println(list);

        // add
        list.add(1,"ylaihui");  // 涉及数据的后移操作
        System.out.println(list);  // [100, ylaihui, ylai, 200, hui]

        // get
        System.out.println(list.get(1)); // ylaihui

        // indexOf
        System.out.println(list.indexOf(200)); // 3
        System.out.println(list.indexOf(1000)); // -1

        // lastIndexOf
        System.out.println(list.lastIndexOf(200)); // -3

        // 重载的remove
        System.out.println(list.remove(2));  // ylai
        System.out.println(list.remove(new Integer(100))); // true
        System.out.println(list);  // [ylaihui, 200, hui]

        // subList
        List list1 = list.subList(1, 3);
        System.out.println(list1);  // [200, hui]
    }
}
