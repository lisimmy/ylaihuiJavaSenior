//SearchListTest.java
package com.ylaihui.list;

import java.util.ArrayList;
import java.util.Iterator;

public class SearchListTest {
    public static void main(String[] args) {
        ArrayList list = new ArrayList();
        list.add(100);
        list.add(200);
        list.add(new String("ylaihui"));

        // 1. 迭代器
        Iterator iterator = list.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }

        // 2. 增强for循环
        for (Object obj : list){
            System.out.println(obj);
        }

        // 3. 普通循环
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
    }
}
