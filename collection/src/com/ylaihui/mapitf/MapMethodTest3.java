//MapMethodTest3.java
package com.ylaihui.mapitf;

import java.util.*;

public class MapMethodTest3 {
    public static void main(String[] args) {
        HashMap map = new HashMap();
        map.put("111","aaa");
        map.put("222","bbb");
        map.put("333","ccc");

        // 通过 Set 集合遍历 所有的key， 通过key获取 value
        Set set = map.keySet();
        Iterator it = set.iterator();
        while(it.hasNext()){
            Object next = it.next();
            Object value = map.get(next);
            System.out.println("key:" + next + ", value:" + value);
        }

        // 遍历所有的value
        Collection values = map.values();
        Iterator iterator = values.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

        // 遍历所有的 Entry，通过Entry的getKey 和 getValue获取 k-v
        Set set1 = map.entrySet();
        Iterator it1 = set1.iterator();
        while (it1.hasNext()){
            Object next = it1.next();
            //System.out.println(next.getClass());
            Map.Entry next1 = (Map.Entry) next;
            System.out.println(next1.getKey() + ":" + next1.getValue());
        }
    }
}
/*
key:111, value:aaa
key:222, value:bbb
key:333, value:ccc
aaa
bbb
ccc
111:aaa
222:bbb
333:ccc
 */
