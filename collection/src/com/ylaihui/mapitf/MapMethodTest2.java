//MapMethodTest2.java
package com.ylaihui.mapitf;

import java.util.HashMap;

public class MapMethodTest2 {
    public static void main(String[] args) {
        HashMap map = new HashMap();
        map.put("111","aaa");
        map.put("222","bbb");
        map.put("333","ccc");
        map.put("444","ddd");

        Object obj = map.get("333");
        System.out.println(obj);  // ccc

        boolean b = map.containsKey("111");
        System.out.println(b);  // true

        b = map.containsValue("ccc");
        System.out.println(b);  // true

        System.out.println(map.size()); // 4

        System.out.println(map.isEmpty());  // false

        HashMap map1 = new HashMap();
        map1.put("333","ccc");
        map1.put("111","aaa");
        map1.put("222","bbb");
        map1.put("444","ddd");

        System.out.println(map.equals(map1)); // true
    }
}
