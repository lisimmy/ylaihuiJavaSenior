//MapMethodTest.java
package com.ylaihui.mapitf;

import java.util.HashMap;

public class MapMethodTest {
    public static void main(String[] args) {
        HashMap map = new HashMap();
        map.put("111","aaa");
        map.put("222","bbb");
        map.put("333","ccc");
        map.put("444","ddd");
        map.put("222","eee");

        System.out.println(map);  // {111=aaa, 222=eee, 333=ccc, 444=ddd}

        HashMap map1 = new HashMap();
        map1.put("111","abc");
        map1.put("222","abc");

        map.putAll(map1);
        System.out.println(map);  // {111=abc, 222=abc, 333=ccc, 444=ddd}

        map.remove("111");
        System.out.println(map);  // {222=abc, 333=ccc, 444=ddd}

        map.clear();
        System.out.println(map.size()); // 0
    }
}
