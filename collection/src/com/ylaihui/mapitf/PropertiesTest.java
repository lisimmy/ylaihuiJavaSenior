//PropertiesTest.java
package com.ylaihui.mapitf;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class PropertiesTest {
    public static void main(String[] args) {
        Properties pr = new Properties();

        try {
            pr.load(new FileInputStream("jdbc.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }

        System.out.println(pr.getProperty("name"));
        System.out.println(pr.getProperty("password"));
    }
}
