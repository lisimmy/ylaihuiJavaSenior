//collectionstool.java
package com.ylaihui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class collectionstool {
    public static void main(String[] args) {
        ArrayList list = new ArrayList();
        list.add(111);
        list.add(222);
        list.add(444);
        list.add(333);

        //reverse(List)：反转 List 中元素的顺序
        Collections.reverse(list);
        System.out.println(list); // [333, 444, 222, 111]

        //shuffle(List)：对 List 集合元素进行随机排序
        Collections.shuffle(list);
        System.out.println(list);  // 随机排序

        //sort(List)：根据元素的自然顺序对指定 List 集合元素按升序排序
        Collections.sort(list);
        System.out.println(list);  // [111, 222, 333, 444]

        //swap(List，int， int)：将指定 list 集合中的 i 处元素和 j 处元素进行交换
        Collections.swap(list, 0, 1);
        System.out.println(list);  // [222, 111, 333, 444]

        //sort(List，Comparator)：根据指定的 Comparator 产生的顺序对 List 集合元素进行排序
        Comparator oc = new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                if(o1 instanceof Integer && o2 instanceof Integer){
                    Integer i1 = (Integer) o1;
                    Integer i2 = (Integer) o2;
                    return -Integer.compare(i1, i2);
                }
                else
                    throw new RuntimeException("不能比较的类型");
            }
        };
        Collections.sort(list, oc);
        System.out.println(list); // [444, 333, 222, 111]  定制排序，从大到小排序
    }
}
