//LinkedHashSetTest.java
package com.ylaihui.setitf;

import java.util.LinkedHashSet;
import java.util.Objects;

class Book{
    int id;
    double price;

    public Book() {
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", price=" + price +
                '}';
    }

    public Book(int id, double price) {
        this.id = id;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return id == book.id &&
                Double.compare(book.price, price) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, price);
    }
}

public class LinkedHashSetTest {
    public static void main(String[] args) {
        LinkedHashSet set = new LinkedHashSet();
        set.add(111);
        set.add(222);
        set.add("ylaihui");
        set.add("ylaihui");
        set.add(new Order(111,100.50));
        set.add(new Order(111,100.50));

        for (Object o : set) {
            System.out.println(o);
        }
    }
}
/*
111
222
ylaihui
Order{id=111, price=100.5}
添加的顺序与遍历的顺序一致，因为底层加了一个双向的链表
*/
