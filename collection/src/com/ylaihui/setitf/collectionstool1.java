//collectionstool1.java
package com.ylaihui.setitf;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class collectionstool1 {
    public static void main(String[] args) {
        ArrayList list = new ArrayList();
        list.add(111);
        list.add(222);
        list.add(444);
        list.add(333);

        //Object max(Collection)：根据元素的自然顺序，返回给定集合中的最大元素
        System.out.println(Collections.max(list));  // 444

        //Object min(Collection)
        System.out.println(Collections.min(list));  // 111

        //Object max(Collection，Comparator): 根据 Comparator 指定的顺序，返回给定集合中的最大元素
        //Object min(Collection，Comparator): 根据 Comparator 指定的顺序，返回给定集合中的最小元素

        //int frequency(Collection，Object)：返回指定集合中指定元素的出现次数
        System.out.println(Collections.frequency(list, 111));  // 1

        // 错误的写法
        //void copy(List dest,List src)：将src中的内容复制到dest中
        //ArrayList dest  = new ArrayList();
        //Collections.copy(dest, list); // IndexOutOfBoundsException: Source does not fit in dest

        List dest = Arrays.asList(new Object[list.size()]);
        Collections.copy(dest, list);
        System.out.println(dest);  // [111, 222, 444, 333]

        //boolean replaceAll(List list，Object oldVal，Object newVal)：使用新值替换 List 对象的所有旧值
        Collections.replaceAll(list, 111, 888);
        System.out.println(list);  // [888, 222, 444, 333]
    }
}
