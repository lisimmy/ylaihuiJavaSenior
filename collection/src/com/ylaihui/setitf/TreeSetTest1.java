//TreeSetTest1.java
package com.ylaihui.setitf;

import java.util.TreeSet;

public class TreeSetTest1 {
    public static void main(String[] args) {
        TreeSet set = new TreeSet();
        // error: 只能添加相同类的对象
        //set.add(111);
        //set.add(222);
        //set.add("aa");

        set.add("y");
        set.add("lai");
        set.add("hui");
        for (Object obj : set) {
            System.out.println(obj);
        }
    }
}