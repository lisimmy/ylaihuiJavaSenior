//collectionstool2.java
package com.ylaihui.setitf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class collectionstool2 {
    public static void main(String[] args) {
        ArrayList list = new ArrayList();
        list.add(111);
        list.add(222);
        list.add(444);
        list.add(333);

        // list1 是线程安全的
        List list1 = Collections.synchronizedList(list);
        System.out.println(list1.getClass());  // class java.util.Collections$SynchronizedRandomAccessList
    }
}
