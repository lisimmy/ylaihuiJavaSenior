//SetItfTest.java
package com.ylaihui.setitf;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;

class Order{
    int id;
    double price;

    public Order() {
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", price=" + price +
                '}';
    }

    public Order(int id, double price) {
        this.id = id;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id &&
                Double.compare(order.price, price) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, price);
    }
}
public class SetItfTest {
    public static void main(String[] args) {
        HashSet hashSet = new HashSet();
        hashSet.add(111);
        hashSet.add(222);
        hashSet.add("ylaihui");
        hashSet.add("ylaihui");
        hashSet.add(new Order(111,100.50));
        hashSet.add(new Order(111,100.50));

        Iterator iterator = hashSet.iterator();
        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }
    }
}
/*
ylaihui
Order{id=111, price=100.5}
222
111
*/
