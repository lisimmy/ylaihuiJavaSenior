//TreeSetTest2.java
package com.ylaihui.setitf;

import java.util.Objects;
import java.util.TreeSet;

class Person{
    String name;
    int age;

    public Person() {
    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age &&
                Objects.equals(name, person.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}

public class TreeSetTest2 {
    public static void main(String[] args) {
        // java.lang.ClassCastException: com.ylaihui.setitf.Person cannot be cast to java.lang.Comparable
        // 放入TreeSet中的对象要求是可比较的，需要实现Comparable接口
        TreeSet set = new TreeSet();
        set.add(new Person("Andy",6));
        set.add(new Person("Caddy",12));
        set.add(new Person("ylaihui",33));
        set.add(new Person("Jim",22));
    }
}
