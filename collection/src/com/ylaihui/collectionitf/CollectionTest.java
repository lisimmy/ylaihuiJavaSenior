//CollectionTest.java
package com.ylaihui.collectionitf;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

public class CollectionTest {
    @Test
    public void testColl(){
        Collection coll = new ArrayList();

        // boolean add(E e);
        coll.add(123);
        coll.add('a');
        coll.add("ylaihui");

        // size() 返回元素的个数
        System.out.println(coll.size()); // 3

        Collection coll2 = new ArrayList();
        coll2.add(new java.lang.String("ylaihui"));
        coll2.add(new java.lang.StringBuffer("ylaihui"));

        //addAll(Collection coll1):将coll1集合中的元素添加到当前的集合中
        coll.addAll(coll2);

        System.out.println(coll.size());  // 5
        // 调用 ArrayList的 toString方法 [123, a, ylaihui, ylaihui, ylaihui]
        System.out.println(coll);

        // 判断集合是否为空 （集合中是否有元素） return size() == 0
        Collection coll3 = new ArrayList();
        System.out.println(coll3.isEmpty());  // true
        System.out.println(coll.isEmpty());   // false

        // 清空集合元素
        coll.clear();
        System.out.println(coll.isEmpty());  // true
    }
}
