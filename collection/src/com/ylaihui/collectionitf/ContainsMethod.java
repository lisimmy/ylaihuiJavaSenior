//ContainsMethod.java
package com.ylaihui.collectionitf;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

class Order {
    private int id;
    private double price;

    public Order() {
    }

    public Order(int id, double price) {
        this.id = id;
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return id == order.id &&
                Double.compare(order.price, price) == 0;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", price=" + price +
                '}';
    }
}

public class ContainsMethod {
    @Test
    public void testMethod() {
        Collection objects = new ArrayList();
        objects.add(123);
        objects.add(false);
        objects.add(new String("ylaihui"));

        // contains(Object obj):判断当前集合中是否包含obj
        // 会调用obj对象所在类的equals(),并非比较的对象的地址，而是比较的对象的内容
        System.out.println(objects.contains(123));  // 自动装箱 true
        System.out.println(objects.contains("ylaihui")); // true
        System.out.println(objects.contains(new String("ylaihui")));  // true

        Order order = new Order(111, 100.00);
        objects.add(order);
        // 不管有没有重写equals，结果都为true，因为是同一个对象 调用== 或 equals都是true
        System.out.println(objects.contains(order));  // true

        // true， 如果Order类没有重写equals方法，结果为 false
        System.out.println(objects.contains(new Order(111, 100.00))); // true

        Collection coll1 = Arrays.asList(123, false);
        // 判断 objects 集合中是否包含集合 coll1, 子集的关系
        System.out.println(objects.containsAll(coll1)); // true

        // remove(Object obj):从当前集合中移除obj元素。
        objects.remove(new Order(111, 100.00));
        System.out.println(objects);  // [123, false, ylaihui]

        //removeAll():移除两个部分的交集(coll2和objects的交集被移除)
        Collection coll2 = Arrays.asList(123, false, 1111);
        objects.removeAll(coll2);
        System.out.println(objects);  // [ylaihui]
    }

    @Test
    public void test3() {
        Collection coll = new ArrayList();
        coll.add(123);
        coll.add(456);
        coll.add(new Order(1111, 100.00));
        coll.add(new String("ylaihui"));
        coll.add(false);

        //retainAll(Collection coll1):交集：获取当前集合和coll1集合的交集，并返回给当前集合
        Collection coll1 = Arrays.asList(123, 456, 789);
        coll.retainAll(coll1);
        System.out.println(coll);  // [123, 456]
    }

    @Test
    public void test4() {
        Collection coll = new ArrayList();
        coll.add(123);
        coll.add(456);
        coll.add(new Order(1111, 100.00));
        coll.add(new String("ylaihui"));
        coll.add(false);

        Collection coll2 = new ArrayList();
        coll2.add(123);
        coll2.add(456);
        coll2.add(new Order(1111, 100.00));
        coll2.add(new String("ylaihui"));
        coll2.add(false);

        //equals(Object obj):要想返回true，需要当前集合和形参集合的元素都相同。
        System.out.println(coll.equals(coll2));  // true
    }

    @Test
    public void test5() {
        Collection coll = new ArrayList();

        coll.add(123);
        coll.add(456);
        coll.add(new Order(1111, 100.00));
        coll.add(new String("ylaihui"));
        coll.add(false);

        //hashCode():返回当前对象的哈希值
        System.out.println(coll.hashCode());

        //集合 -> 数组：toArray()
        Object[] objects = coll.toArray();
        for (int i = 0; i < objects.length; i++) {
            System.out.println(objects[i]);
        }

        //数组 -> 集合:调用Arrays类的静态方法asList(),基本数据类型的数组无法转换为多元素的集合
        Collection coll2 = Arrays.asList(objects);
        System.out.println(coll2);  // [[123, 456, Order{id=1111, price=100.0}, ylaihui, false]]
        System.out.println("coll2.size() = " + coll2.size());  // 5

        Collection coll3 = Arrays.asList(new String[]{"y", "lai", "hui"});
        System.out.println(coll3);  // [y, lai, hui]
        System.out.println(coll3.size()); // 3

        Collection coll4 = Arrays.asList(new int[]{1, 2, 3});
        System.out.println(coll4);  // [[I@22927a81]
        System.out.println(coll4.size()); // 1

        Collection coll5 = Arrays.asList(new Integer[]{1, 2, 3});
        System.out.println(coll5);  // [1, 2, 3]
        System.out.println(coll5.size()); // 3
    }
}
