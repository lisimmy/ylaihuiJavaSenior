//IteratorTest.java
package com.ylaihui.collectionitf;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class IteratorTest {
    @Test
    public void testIterator(){
        Collection coll = new ArrayList();
        coll.add(123);
        coll.add(234);
        coll.add("ylaihui");

        Iterator iterator = coll.iterator();

        while(iterator.hasNext()){
            System.out.println(iterator.next());
        }

        // 迭代器遍历结束后，需要重新初始化 否则报异常 NoSuchElementException
        // 可以这样实现，但是不推荐这么使用
        iterator = coll.iterator();
        for (int i = 0; i < coll.size(); i++) {
            System.out.println(iterator.next());
        }
    }
}
