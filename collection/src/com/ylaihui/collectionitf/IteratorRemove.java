//IteratorRemove.java
package com.ylaihui.collectionitf;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class IteratorRemove {
    @Test
    public void testremove(){
        Collection coll = new ArrayList();
        coll.add(111);
        coll.add(222);
        coll.add("ylaihui");

        Iterator it = coll.iterator();
        while(it.hasNext()){
            Object next = it.next();
            if(next.equals(111)){
                it.remove();
            }
        }
        it = coll.iterator();
        while (it.hasNext())
            System.out.println(it.next());
    }
}
