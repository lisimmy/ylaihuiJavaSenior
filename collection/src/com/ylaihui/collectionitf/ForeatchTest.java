//ForeatchTest.java
package com.ylaihui.collectionitf;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collection;

public class ForeatchTest {
    @Test
    public void testremove() {
        Collection coll = new ArrayList();
        coll.add(111);
        coll.add(222);
        coll.add("ylaihui");

        for(Object obj : coll){
            System.out.println(obj);
        }
    }

    @Test
    public void testArray(){
        int[] ints = {1, 2, 3, 4, 5};

        for(int i : ints){
            System.out.println(i);
        }
    }
}
