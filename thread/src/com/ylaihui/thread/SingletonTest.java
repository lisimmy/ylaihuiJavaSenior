//SingletonTest.java
package com.ylaihui.thread;

class ProjectProPrities{
    private static ProjectProPrities instance = null;

    // 效率差：举例
    // 假如目前2个线程进行getInstance，那么其中一个线程会进入互斥区域，并创建了实例，另一个线程等待锁释放，实例创建完成后释放锁，
    // 另一个线程进入互斥区，直接获取实例（不用再次new了）
    // 如果此时又有2个线程进行getInstance，那么也会进行锁的等待，但是此时实例已经有了， 实际上是不需要等待的
//    public static ProjectProPrities getInstance(){
//        synchronized(ProjectProPrities.class){
//            if (instance == null) {
//                instance = new ProjectProPrities();
//            }
//        }
//        return instance;
//    }
    // 优化以后版本，不存在上述的效率低的问题
    public static ProjectProPrities getInstance(){
        if(instance == null) {
            synchronized(ProjectProPrities.class){
                if (instance == null) {
                    instance = new ProjectProPrities();
                }
            }
        }
        return instance;
    }
}

public class SingletonTest {
    public static void main(String[] args) {
        ProjectProPrities instance = ProjectProPrities.getInstance();

        System.out.println(instance);
    }
}
