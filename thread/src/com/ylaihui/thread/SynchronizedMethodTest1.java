//SynchronizedMethodTest1.java
package com.ylaihui.thread;

class ProcMsg extends Thread{
    private static int message = 100;
    @Override
    public void run() {
        while(true){
            proc();
        }
    }
//    private synchronized void proc(){ // 同步监视器是 this， 是 t1 t2 t3
    private static synchronized void proc(){ // 同步监视器是 ProcMsg.class 对象
        if(message > 0){
            System.out.println(Thread.currentThread().getName() + ":" + message);
            message--;
        }
    }
}
public class SynchronizedMethodTest1 {
    public static void main(String[] args) {
        ProcMsg t1 = new ProcMsg();
        ProcMsg t2 = new ProcMsg();
        ProcMsg t3 = new ProcMsg();
        t1.start();
        t2.start();
        t3.start();
    }
}
