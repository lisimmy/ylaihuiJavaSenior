//CreateThreadRunnableInf.java
package com.ylaihui.thread;

// 1. 创建一个实现了Runnable接口的类
class ThreadR implements Runnable {
    //2. 实现类去实现Runnable中的抽象方法：run()
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
    }
}

public class CreateThreadRunnableInf {
    public static void main(String[] args) {
        // 3. 创建实现类的对象
        ThreadR t1 = new ThreadR();

        // 4. 将此对象作为参数传递到Thread类的构造器中，创建Thread类的对象
        Thread t2 = new Thread(t1);
        Thread t3 = new Thread(t1);

        // 5. 通过Thread类的对象调用start()
        t2.start();  // 调用了线程Thread类中的target类的run方法
        t3.start();
    }
}
