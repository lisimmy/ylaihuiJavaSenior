//ThreadPriorityTest.java
package com.ylaihui.thread;

class ThreadPriority extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            System.out.println(Thread.currentThread().getName()+ ":" + i);
        }
    }
}

public class ThreadPriorityTest {
    public static void main(String[] args) {
        ThreadPriority t1 = new ThreadPriority();
        // t1 设置为最高优先级， 主线程设置为最低优先级
        t1.setPriority(Thread.MAX_PRIORITY);
        Thread.currentThread().setPriority(Thread.MIN_PRIORITY);

        t1.start();

        for (int i = 0; i < 100; i++) {
            System.out.println(Thread.currentThread().getName()+ ":" + i);
        }
    }
}
