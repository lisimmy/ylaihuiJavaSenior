//ProcessMessageTest.java
package com.ylaihui.thread;

class ProcessMessage implements Runnable{
    private int message = 100;
    @Override
    public void run() {
        while(true) {
            if(message > 0) {
                System.out.println("process message: " + message);
                message--;
            }else
                break;
        }
    }
}

public class ProcessMessageTest {
    public static void main(String[] args) {
        ProcessMessage p = new ProcessMessage();
        Thread t1 = new Thread(p);
        Thread t2 = new Thread(p);
        Thread t3 = new Thread(p);

        t1.start();
        t2.start();
        t3.start();
    }
}
