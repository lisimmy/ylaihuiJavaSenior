//CreateThreadSubclass.java
package com.ylaihui.thread;

public class CreateThreadSubclass {
    public static void main(String[] args) {
        new Thread(){
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
            }
        }.start();

        new Thread(){
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
            }
        }.start();

        System.out.println(Thread.currentThread().getName());
    }
}
