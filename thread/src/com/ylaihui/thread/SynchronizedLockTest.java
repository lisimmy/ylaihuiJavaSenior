//SynchronizedLockTest.java
package com.ylaihui.thread;

import java.util.concurrent.locks.ReentrantLock;

class ProcPicture implements Runnable{
    private static int num = 100;
    private ReentrantLock lock = new ReentrantLock();
    @Override
    public void run() {
        while(true) {
            try{
                lock.lock();
                if (num > 0) {
                    System.out.println(Thread.currentThread().getName() + ":" + num);
                    num--;
                } else
                    break;
            }finally {
                // 保证了释放锁一定能执行
                lock.unlock();
            }
        }
    }
}

public class SynchronizedLockTest {
    public static void main(String[] args) {
        ProcPicture p = new ProcPicture();
        Thread t1 = new Thread(p);
        Thread t2 = new Thread(p);
        Thread t3 = new Thread(p);

        t1.start();
        t2.start();
        t3.start();
    }
}
