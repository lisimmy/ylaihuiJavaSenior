//ThreadMethod.java
package com.ylaihui.thread;

class TestThreadMethod extends Thread{
    public TestThreadMethod(){}
    public TestThreadMethod(String name) {
        super(name);
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName());
    }
}

public class ThreadMethod {
    public static void main(String[] args) {
        TestThreadMethod t1 = new TestThreadMethod();
        t1.setName("Thread---1");
        t1.start();

        TestThreadMethod t2 = new TestThreadMethod("Thread---2");
        t2.start();

        Thread.currentThread().setName("Thread---main");
        System.out.println(Thread.currentThread().getName());
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(t1.isAlive());
        System.out.println(t2.isAlive());
    }
}
