//CreateThreadError.java
package com.ylaihui.thread;

class ProcessNumber1 extends Thread{
    @Override
    public void run() {
        for (int i = 0; i < 100; i++) {
            if( i % 5 == 0)
                System.out.println(i);
        }
    }
}

public class CreateThreadError {
    public static void main(String[] args) {
        ProcessNumber1 processNumber = new ProcessNumber1();
        processNumber.start();// 子线程执行 run() 方法中的代码
//        processNumber.start(); // IllegalThreadStateException
        // 主线程执行以下代码
        for (int i = 100; i < 200; i++) {
            if(i % 5 == 0)
                System.out.println(i);
        }
    }
}
