//ThreadSynchronizedBlock.java
package com.ylaihui.thread;

class ProcessMessage1 implements Runnable{
    private int message = 100;
    Object obj = new Object();
    @Override
    public void run() {

//        synchronized(obj){
        synchronized(this){
            while(true){
                if(message > 0){
                    System.out.println("process message: "+ message);
                    message--;
                }else
                    break;
            }
        }
    }
}

public class ThreadSynchronizedBlock {
    public static void main(String[] args) {
        ProcessMessage1 p = new ProcessMessage1();
        Thread t1 = new Thread(p);
        Thread t2 = new Thread(p);
        Thread t3 = new Thread(p);

        t1.start();
        t2.start();
        t3.start();
    }
}
