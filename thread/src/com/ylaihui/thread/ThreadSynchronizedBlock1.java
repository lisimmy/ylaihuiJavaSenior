//ThreadSynchronizedBlock1.java
package com.ylaihui.thread;

class ProcessMsg extends Thread{
    private static int message = 100;
    private static Object obj = new Object();
    @Override
    public void run() {
        // 方式一
//        synchronized(obj){
        // ProcessMsg 在运行时只加载一次， 在底层也是一个对象
        synchronized(ProcessMsg.class){
            while(true){
                if(message > 0 ){
                    System.out.println("process message: " + message);
                    message--;
                }else
                    break;
            }
        }
    }
}

public class ThreadSynchronizedBlock1 {
    public static void main(String[] args) {
        ProcessMsg t1 = new ProcessMsg();
        ProcessMsg t2 = new ProcessMsg();
        ProcessMsg t3 = new ProcessMsg();

        t1.start();
        t2.start();
        t3.start();
    }
}
