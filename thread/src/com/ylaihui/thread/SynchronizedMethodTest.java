//SynchronizedMethodTest.java
package com.ylaihui.thread;

import static java.lang.Thread.sleep;

class ProcessMessage2 implements Runnable{
    private int message = 100;
    @Override
    public void run() {
        while(true) {
            proc();
        }
    }
    private synchronized void proc(){  // 同步监视器是 this
        try {
            sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if(message > 0) {
            System.out.println("process message: " + Thread.currentThread().getName() + "-" +  message);
            message--;
        }
    }
}

public class SynchronizedMethodTest {
    public static void main(String[] args) {
        ProcessMessage2 p = new ProcessMessage2();
        Thread t1 = new Thread(p);
        Thread t2 = new Thread(p);
        Thread t3 = new Thread(p);

        t1.start();
        t2.start();
        t3.start();
    }
}
