//ThreadPoolTest.java
package com.ylaihui.thread1;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

class SumThread implements Runnable{

    @Override
    public void run() {
        int sum = 0;
        for (int i = 1; i <=10; i++) {
            sum += i;
        }
        System.out.println(sum);
    }
}

public class ThreadPoolTest {
    public static void main(String[] args) {
        // 创建线程池
        ExecutorService pool = Executors.newFixedThreadPool(10);
        System.out.println(pool.getClass());
        ThreadPoolExecutor p = (ThreadPoolExecutor) pool;
//        p.setCorePoolSize(20);
//        p.setKeepAliveTime();
//        p.setMaximumPoolSize(30);
        pool.execute(new SumThread()); // Runnable接口方式

//        pool.submit(); Callable接口方式
        pool.shutdown();

    }
}
