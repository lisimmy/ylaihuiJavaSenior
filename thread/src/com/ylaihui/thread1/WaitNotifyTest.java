//WaitNotifyTest.java
package com.ylaihui.thread1;

class ProcMsg implements Runnable{
    private static int message = 10;
    private Object obj = new Object();
    @Override
    public void run() {

        while(true){
            //wait()，notify()，notifyAll()三个方法的调用者必须是同步代码块或同步方法中的同步监视器
            // （都写为obj或都写为this） 否则报异常 IllegalMonitorStateException
            //synchronized (obj){ // IllegalMonitorStateException
            synchronized (this){
                // 一旦执行此方法，就会唤醒被wait的一个线程。如果有多个线程被wait，就唤醒优先级高的那个。
                this.notify();
                if(message > 0)
                {
                    System.out.println(Thread.currentThread().getName() + ":" + message);
                    message--;
                    try {
                        // 一旦执行此方法，当前线程就进入阻塞状态，并释放锁（同步监视器）。
                        this.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }else
                    break;
            }
        }
    }
}

public class WaitNotifyTest {
    public static void main(String[] args) {
        ProcMsg procMsg = new ProcMsg();

        Thread t1 = new Thread(procMsg);
        Thread t2 = new Thread(procMsg);

        t1.start();
        t2.start();
    }
}
