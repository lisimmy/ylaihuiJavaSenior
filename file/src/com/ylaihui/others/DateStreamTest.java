//DateStreamTest.java
package com.ylaihui.others;

import org.junit.Test;

import java.io.*;

public class DateStreamTest {
    @Test
    public void test(){
        DataOutputStream dos = null;
        try {
            dos = new DataOutputStream(new FileOutputStream("dataoutput.txt"));

            dos.writeUTF("猿来绘");
            //刷新操作，内存中的数据写入文件
            dos.flush();
            dos.writeInt(111);
            dos.flush();
            dos.writeBoolean(true);
            dos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //3.
            if (dos != null) {
                try {
                    dos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Test
    public void test2(){
        DataInputStream dis = null;
        try {
            dis = new DataInputStream(new FileInputStream("dataoutput.txt"));

            String name = dis.readUTF();
            int age = dis.readInt();
            boolean isMale = dis.readBoolean();

            System.out.println("name = " + name);
            System.out.println("age = " + age);
            System.out.println("isMale = " + isMale);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (dis != null) {
                try {
                    dis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
