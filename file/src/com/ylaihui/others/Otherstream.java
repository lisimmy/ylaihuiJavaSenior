//otherstream.java
package com.ylaihui.others;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Otherstream {
    public static void main(String[] args) {
        BufferedReader br = null;
        try {
            InputStreamReader isr = new InputStreamReader(System.in);
            br = new BufferedReader(isr);

            String str = null;
            while ((str = br.readLine()) != null) {
                if ("q".equalsIgnoreCase(str) || "quit".equalsIgnoreCase(str)) {
                    break;
                } else
                    System.out.println(str.toUpperCase());
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
