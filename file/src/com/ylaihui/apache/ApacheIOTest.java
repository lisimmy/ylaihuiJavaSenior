//ApacheIOTest.java
package com.ylaihui.apache;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

public class ApacheIOTest {
    public static void main(String[] args) {
        File file1 = new File("file\\ylaihui.txt");
        File file2 = new File("file\\ylaihui-apache.txt");

        try {
            FileUtils.copyFile(file1,file2);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
