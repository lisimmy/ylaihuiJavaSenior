//ByteCharTrans.java
package com.ylaihui.trans;

import org.junit.Test;

import java.io.*;

public class ByteCharTrans {
    @Test
    public void test(){
        //1. new File
        FileInputStream fr = null;
        FileOutputStream fw = null;
        try {
            File file1 = new File("ylaihuishopping.txt");
            File file2 = new File("ylaihuishopping-GBK.txt");

            //2. new byte stream
            fr = new FileInputStream(file1);
            fw = new FileOutputStream(file2);

            //3. new char stream
            InputStreamReader isr = new InputStreamReader(fr,"UTF-8");
            OutputStreamWriter osw = new OutputStreamWriter(fw,"GBK");

            //4. read write
            char[] chars = new char[10];
            int len = 0;
            while ((len = isr.read(chars)) != -1){
                osw.write(chars,0,len);
                osw.flush();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //5.close file
            if (fw != null) {
                try {
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fr != null) {
                try {
                    fr.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
