//FileWriterTest.java
package com.ylaihui.file;

import org.junit.Test;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriterTest {
    @Test
    public void testWriter(){
        FileWriter fileWriter = null;
        try {
            // 1. File
            File file = new File("ylai.txt");

            // 2. FileWriter
            fileWriter = new FileWriter(file,true);

            // 3.write
            fileWriter.write("www.ylaihui.com\n");
            fileWriter.write("Welcome!\n");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileWriter != null) {
                    // 4.close
                    fileWriter.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
