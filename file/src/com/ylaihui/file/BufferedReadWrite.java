//BufferedReadWrite.java
package com.ylaihui.file;

import org.junit.Test;

import java.io.*;

public class BufferedReadWrite {
    @Test
    public void test() {

        BufferedReader br = null;
        BufferedWriter bw = null;
        try {
            br = new BufferedReader(new FileReader(new File("ylaihui.txt")));
            bw = new BufferedWriter(new FileWriter(new File("ylaihuidest.txt")));

            //char[] chars = new char[1024];
            //int len = 0;
            //while ((len = br.read(chars)) != -1){
            //    bw.write(chars,0,len);
            //}
            String temp;
            // readLine 不包括换行符
            while ((temp = br.readLine()) != null) {
                bw.write(temp);
                bw.newLine();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                bw.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }
}
