//CopyFileTest.java
package com.ylaihui.file;

import org.junit.Test;

import java.io.*;

public class CopyFileTest {
    @Test
    public void test(){
        FileReader fileReader = null;
        FileWriter fileWriter = null;
        try {
            //1. new File
            File filesrc = new File("ylaihui.txt");
            File destfile = new File("dest.txt");

            //2. new FileReader and FileWriter
            fileReader = new FileReader(filesrc);
            fileWriter = new FileWriter(destfile,true);

            //3. read and write
            char[] chars = new char[10];
            int len = 0;
            while((len = fileReader.read(chars)) != -1){
                fileWriter.write(chars,0,len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //4. close file stream
            try {
                fileReader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                fileWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
