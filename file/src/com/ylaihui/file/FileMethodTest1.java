//FileMethodTest1.java
package com.ylaihui.file;

import org.junit.Test;

import java.io.File;
import java.io.IOException;

public class FileMethodTest1 {
    public static void main(String[] args) {
        System.out.println("#1 test file method");
        File src = new File("file\\ylaihui.txt");
        System.out.println(src.getAbsolutePath());
        System.out.println(src.getPath());
        System.out.println(src.getName());
        System.out.println(src.getParent());
        System.out.println(src.length());
        System.out.println(src.lastModified());

        System.out.println("#2 test file method");
        File file = new File("file");
        String[] list = file.list();
        for (String s : list) {
            System.out.println(s);
        }

        System.out.println("#3 test file method");
        File file1 = new File("file");
        File[] files = file1.listFiles();
        for (File fs : files) {
            System.out.println(fs);
        }
    }

    @Test
    public void test() {
        File file1 = new File("ylaihui.txt");
        File file2 = new File("D:\\ylaihui\\temp.txt");

        boolean renameTo = file2.renameTo(file1);
        System.out.println(renameTo);
    }

    @Test
    public void test1() {

        File file1 = new File("ylaihui.txt");

        System.out.println(file1.isDirectory());
        System.out.println(file1.isFile());
        System.out.println(file1.exists());
        System.out.println(file1.canRead());
        System.out.println(file1.canWrite());
        System.out.println(file1.isHidden());
    }

    @Test
    public void test2() {
        // 文件存在则删除，文件不存在则创建
        File file = new File("ylaihui.properties");
        if (file.exists()) {
            file.delete();
        } else {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
