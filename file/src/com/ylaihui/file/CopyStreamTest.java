//CopyFileTest.java
package com.ylaihui.file;

import org.junit.Test;

import java.io.*;

public class CopyStreamTest {
    @Test
    public void test() {
        FileInputStream fi = null;
        FileOutputStream fo = null;
        try {
            //1. new File
            File filesrc = new File("src.PNG");
            File destfile = new File("dest.PNG");

            //2. new
            fi = new FileInputStream(filesrc);
            fo = new FileOutputStream(destfile);

            //3. read and write
            byte[] bytes = new byte[10];
            int len = 0;
            while ((len = fi.read(bytes)) != -1) {
                fo.write(bytes, 0, len);
            }
            System.out.println("copy file succ!");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //4. close file stream
            try {
                fi.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                fo.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
