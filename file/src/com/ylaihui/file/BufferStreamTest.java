//BufferStreamTest.java
package com.ylaihui.file;

import org.junit.Test;

import java.io.*;

public class BufferStreamTest {
    @Test
    public void test(){
        BufferedInputStream bsrc = null;
        BufferedOutputStream bdest = null;
        try {
            //1. new File()
            File filesrc = new File("thread_status.png");
            File filedest = new File("thread_statusdest.png");

            //2. new Stream
            FileInputStream fsrc = new FileInputStream(filesrc);
            FileOutputStream fdest = new FileOutputStream(filedest);

            bsrc = new BufferedInputStream(fsrc);
            bdest = new BufferedOutputStream(fdest);

            //3. read and write
            byte[] bytes = new byte[10];
            int len = 0;
            while((len = bsrc.read(bytes)) != -1){
                bdest.write(bytes,0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //4. close
            if (bdest != null) {
                try {
                    bdest.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (bsrc != null) {
                try {
                    bsrc.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
