//FileReaderWriterTest.java
package com.ylaihui.file;

import org.junit.Test;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class FileReaderWriterTest1 {
    @Test
    public void test1(){
        FileReader fileReader = null;
        try {
            //1.实例化File类的对象，指明要操作的文件
            File file = new File("ylaihui.txt");

            //2.指定具体的流
            fileReader = new FileReader(file);

            //3. 数据的读入
            int data = 0;
            while ((data = fileReader.read()) != -1){
                System.out.print((char) data);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            //4. 关闭流
            // 问题：如果在read时， 出现了异常，那么会导致流未关闭，可能会导致栈溢出
            try {
                // 如果在步骤2.指定具体的流的时候报异常的时候，fileReader为空
                if (fileReader != null) {
                    fileReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
