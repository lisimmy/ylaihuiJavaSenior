//FileTest1.java
package com.ylaihui.file;

import java.io.File;

public class FileTest1 {
    public static void main(String[] args) {
        File file1 = new File("d:\\ylaihui\\ylaihui.txt");
        File file2 = new File("d:\\ylaihui", "ylaihui.txt");
        File file3 = new File(new File("D:\\ylaihui"), "ylaihui.txt");

        System.out.println(file1);
        System.out.println(file2);
        System.out.println(file3);

        // 你平台的文件目录 分隔符 win \\ linux unix /
        System.out.println(File.separator);
    }
}
