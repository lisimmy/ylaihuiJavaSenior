//ObjectStramTest.java
package ObjectStream;

import org.junit.Test;

import java.io.*;

class Person implements Serializable{
    public static final long serialVersionUID = 21214546464212L;
}

public class ObjectStramTest {
    @Test
    public void test() {
        ObjectOutputStream oos = null;
        try {
            //1. new ObjectStream
            oos = new ObjectOutputStream(new FileOutputStream("ylaihui.dat"));

            oos.writeObject(new String("ylaihui"));
            oos.writeObject(new Person());

            oos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Test
    public void test1(){
        ObjectInputStream ois = null;
        try {
            ois = new ObjectInputStream(new FileInputStream("ylaihui.dat"));

            String o = (String) ois.readObject();
            Person p = (Person) ois.readObject();

            System.out.println(o);
            System.out.println(p);
            System.out.println(p.getClass());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
/*
ylaihui
ObjectStream.Person@16b3fc9e
class ObjectStream.Person
 */

